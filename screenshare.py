import socket

from PIL import ImageGrab


HOST = "192.168.0.96"
PORT = 1337
X_OFFSET = 500
Y_OFFSET = 200
DOWNSIZE = 6


def get_payload():
    image = ImageGrab.grab()
    image = image.resize((image.width // DOWNSIZE, image.height // DOWNSIZE))
    payload = set()
    for x in range(image.width):
        for y in range(image.height):
            rgb = bytes(image.getpixel((x, y))).hex()
            payload.add(f"PX {X_OFFSET + x} {Y_OFFSET + y} {rgb}")
    return payload


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))

last_payload = get_payload()
while True:
    payload = get_payload() - last_payload
    s.send("\n".join(payload).encode() + b"\n")
    last_payload = payload
